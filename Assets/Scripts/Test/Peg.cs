﻿using UnityEngine;
using System.Collections;

public class Peg : MonoBehaviour {
	Vector3 topPeg;
	GameObject player;

	void Start() {
		player = GameObject.FindGameObjectWithTag("Hero");
	}

	void OnEnable() {
		topPeg = transform.position + new Vector3(0, GetComponent<BoxCollider>().bounds.size.y/2, 0);
	}
	

	void Update () {
		DestroyPeg();
	}


	public Vector3 GetTopPeg() {
		return topPeg;
	}


	void DestroyPeg() {
		if (transform.position.x < player.transform.position.x - GameSettings.DISTANCE_TO_DESTROY) {
			PegPooler.getPooler.RemoveFromList(gameObject);
			Destroy(gameObject);
//			gameObject.SetActive(false);
		}
	}

}
