﻿using UnityEngine;
using System.Collections;

public class RotatePlayerTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Rotate(-9);
	}


	void Rotate(float angle) {
		transform.RotateAround(transform.position, Vector3.forward, angle);
	}
}
