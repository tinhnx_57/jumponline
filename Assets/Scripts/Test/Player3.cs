﻿using UnityEngine;
using System.Collections;

public class Player3 : MonoBehaviour {

	public enum PlayerState {
		Start,
		FallOnLine,
		MovingDirect,
		FallDown,
		Stretch,
		Jump,
		JumpDown,
		Die
	}
	public bool isNearTopPeg2 {get; private set;}
	public PegPooler pegPooler;
	public float radius {get; private set;}
	public PlayerState state {get; private set;}
	Vector3 vec0;
	Vector3 vecCur;
	Vector3 vecPrev;
	bool isJumpNext; //jump without mouse click
	float yStartJump2;
	float x;
	float widthFall;
	float heightFall;
	float deltaX;
	float maxHoldPositionX;
	public float widthJump {get; private set;}
	float heightJump;
	float widthParabolWhenClick;
	public Vector3 pointMeetLineCur{get; private set;}
	public Vector3 pointMeetLinePrev {get; private set;}
	public Vector3 pointMeetLineNext {get; private set;}

	void Start () {
		Init();
		radius = GetComponent<SphereCollider>().bounds.size.x/2;
		state = PlayerState.Start;
		vec0 = transform.position;
	}

	void Init() {
		widthFall = GameSettings.WIDTH_FALL;
		heightFall = GameSettings.HEIGHT_FALL;
		x = 0;
	}

	void Update () {
		Moving();
		UpdateState();
//		print(state);
		print(Time.deltaTime);
		CheckPlayerNearTopPeg2();
	}


	Vector3  GetPointMeetLine(Vector3 playerPos) {
		Vector3 bottomPos = playerPos - new Vector3(0, radius, 0);
		float xPos = (bottomPos.x - playerPos.x) * Mathf.Cos(pegPooler.alphaTop1Top2) - (bottomPos.y - playerPos.y) * Mathf.Sin(pegPooler.alphaTop1Top2) + playerPos.x;
		float yPos = (bottomPos.x - playerPos.x) * Mathf.Sin(pegPooler.alphaTop1Top2) + (bottomPos.y - playerPos.y) * Mathf.Cos(pegPooler.alphaTop1Top2) + playerPos.y;
		return new Vector3(xPos, yPos, playerPos.z);
	}


	void UpdateState() {
		if ((state == PlayerState.FallOnLine || state == PlayerState.MovingDirect) && transform.position.x > pegPooler.top2.x 
			&& transform.position.y - radius > pegPooler.top2.y) {
			vec0 = new Vector3(transform.position.x - widthFall/2, transform.position.y - heightFall, transform.position.z);
			x = widthFall/2;
			state = PlayerState.FallDown;
			pegPooler.ChangeShowLine(true);
		}
	}


	void CheckPlayerNearTopPeg2() {
		if (pegPooler.top2.x - transform.position.x < 4 * radius) {
			isNearTopPeg2 = true;
		}
		else {
			isNearTopPeg2 = false;
		}
	}

	void Moving() {
		deltaX = GameSettings.PLAYER_MOVING_SPEED * Time.deltaTime;
		switch (state) {
		case PlayerState.Start:
			vecPrev = transform.position;
			pointMeetLinePrev = GetPointMeetLine(vecPrev);
			vecCur = transform.position - new Vector3(0, 0.8f, 0);
			transform.position = vecCur;
			pointMeetLineCur = GetPointMeetLine(vecCur);
			Rotate(-10);
			break;
		case PlayerState.FallOnLine:
			vecCur = new Vector3(vec0.x  + x, vec0.y + pegPooler.parabolA * x * x +  pegPooler.parabolB * x, transform.position.z);
			float xPosFallOnLine = (vecCur.x - vec0.x) * Mathf.Cos(pegPooler.alpha2Peg) - (vecCur.y - vec0.y) * Mathf.Sin(pegPooler.alpha2Peg) + vec0.x;
			float yPosFallOnLine = (vecCur.x - vec0.x) * Mathf.Sin(pegPooler.alpha2Peg) + (vecCur.y - vec0.y) * Mathf.Cos(pegPooler.alpha2Peg) + vec0.y;
			transform.position = new Vector3(xPosFallOnLine, yPosFallOnLine, transform.position.z);
			x += deltaX * 3.5f;
			Rotate(-10);
			break;
		case PlayerState.MovingDirect://when jump down near top peg 2
			vecCur = new Vector3(vec0.x + x, vec0.y - pegPooler.lineAfterJumpA * x / pegPooler.lineAfterJumpB, transform.position.z);
			transform.position = vecCur;
			x += deltaX * 2f;
			Rotate(-10);
			break;
		case PlayerState.FallDown:
			vecPrev = transform.position;
			pointMeetLinePrev = GetPointMeetLine(vecPrev);
			vecCur = new Vector3(vec0.x + x, vec0.y + GetAFall() * (x * x) + GetBFall() * x, transform.position.z);
			transform.position = vecCur;
			pointMeetLineCur = GetPointMeetLine(vecCur);
			x += deltaX;
			Rotate(-10);
			break;
		case PlayerState.Stretch:
			transform.position = Vector3.Lerp(transform.position, new Vector3(GetPositionHoldMaxX(), 
				pegPooler.GetPositionYWhenHoldMax(), transform.position.z), 0.09f);
			Rotate(-10);
			pointMeetLineNext = GetPointMeetLine(transform.position); //use for jump state fixed error first jump
			break;
		case PlayerState.Jump:
			vecPrev = transform.position;
			pointMeetLinePrev = GetPointMeetLine(vecPrev);
			vecCur = new Vector3(vec0.x + x, vec0.y + GetAJump() * (x * x) + GetBJump() * x, transform.position.z);
			transform.position = vecCur;
			pointMeetLineCur = GetPointMeetLine(vecCur);
			if (isJumpNext) {
				x += deltaX * 2.7f;
			}
			else {
				x += deltaX * 5.5f;
			}
			Vector3 playerNextJump = new Vector3(vec0.x + x, vec0.y + GetAJump() * (x * x) + GetBJump() * x, transform.position.z);
			pointMeetLineNext = GetPointMeetLine(playerNextJump);
			if (x > widthJump/2) {
				if (isJumpNext) {
					if (pegPooler.RelativeWithLine(pointMeetLineCur) < 0) {
						state = PlayerState.JumpDown;
					}
					else {
						pegPooler.ChangePlayerToFallOnLineState();
						GamePlayTest.current.ChangeStartFallOnLineValue(true);
					}

				}
				else {
					state = PlayerState.JumpDown;
				}
			}
			Rotate(-10);
			break;
		case PlayerState.JumpDown:
			vecPrev = transform.position;
			pointMeetLinePrev = GetPointMeetLine(vecPrev);
			vecCur = new Vector3(vec0.x + x, vec0.y + GetAJump() * (x * x) + GetBJump() * x, transform.position.z);
			transform.position = vecCur;
			pointMeetLineCur = GetPointMeetLine(vecCur);
			if (isJumpNext) {
				x += deltaX * 2.7f;
			}
			else {
				x += deltaX * 5.5f;
			}
			Vector3 playerNextJumpDown = new Vector3(vec0.x + x, vec0.y + GetAJump() * (x * x) + GetBJump() * x, transform.position.z);
			pointMeetLineNext = GetPointMeetLine(playerNextJumpDown);
			Rotate(-10);
			break;
		case PlayerState.Die:
			break;
		}
	}

	void OnTriggerEnter(Collider col) {
		if (col.tag == "Obstacle") {
			state = PlayerState.Die;
			col.GetComponent<Obstacle>().SetObstacleWhenGameOver();
			GamePlayTest.current.SetGameOver();
		}
	}
		
	void Rotate(float angle) {
		transform.RotateAround(transform.position, Vector3.forward, angle * Time.deltaTime * 20);
	}

	public void CreateJumpParabol(float force) {
		if (force < 0.2f) {
			force = 1f;
		}
		else {
			force += 1f;
		}

		heightJump = force * 9.0f;
		widthJump = force * 12.0f;
	}
		

	public void SetPositionHoldMaxX(float max) {
		maxHoldPositionX = max;
	}

	public float GetPositionHoldMaxX() {
		return maxHoldPositionX;
	}

	public Vector3 GetVecPrev() {
		return vecPrev;
	}

	public void SetState(PlayerState toState) {
		state = toState;
	}

	public void SetVec0(Vector3 newVec0) {
		vec0 = newVec0;
	}

	public void ResetX() {
		x = 0;
	}

	float GetAFall() { //coefficient a of parabol when falldown
		return -4 * heightFall/ (widthFall * widthFall);
	}

	float GetBFall() {
		return 4 * heightFall / widthFall;
	}

	float GetAJump() { //coefficient a of parabol when jump state
		return -4 * heightJump / (widthJump * widthJump);
	}

	float GetBJump() {
		return 4 * heightJump / widthJump;
	}


	public void CreateJumpNextParabol() { //jump without mouse click
		if (widthJump == 0) { // when first jump without mouse click
			widthJump = 3;
		}
		else {
			widthJump = widthJump / 4; // relative GetTopParabolNext() function, if change here, must change 4 in that function
		}
		heightJump = widthJump / 3.1f;
	}

	public void SetIsJumpNext(bool jumpNext) {
		isJumpNext = jumpNext;
	}

	public Vector3 GetTopParabolNext() { //relative with CreateJumpNextParabol()
		float widthJumpNext = widthJump / 4;
		float heightJumpNext = widthJump / 3.1f;
		float a = -4 * heightJumpNext / (widthJumpNext * widthJumpNext);
		float b = 4 * heightJumpNext / widthJumpNext;
		float xJumpNext = widthJumpNext/2;
		Vector3 rootVec = transform.position;
		Vector3 topNext = new Vector3(rootVec.x + xJumpNext, rootVec.y + a * xJumpNext * xJumpNext + b * xJumpNext, transform.position.z);

		return topNext;
	}


}
