﻿using UnityEngine;
using System.Collections;

public class Coroutine : MonoBehaviour {
	int a;
	// Use this for initialization
	void Start () {
		a = 0;
		StartCoroutine(waits());
	}

	void test() {
		Debug.Log("done");
	}

	IEnumerator waits() {
		yield return StartCoroutine(DoSomething());	
		test();
	}

	IEnumerator DoSomething() {
		for (int i = 0; i < 5; i++) {
			a = a+i;
			print(a);
			yield return new WaitForSeconds(1);
		}

	}

}
