﻿using UnityEngine;
using System.Collections;

public class MyPlayerTest : MonoBehaviour {
	public GameObject platform;
	public GamePlayControl gamePlay;
	bool collision;

	float x = 0;
	float d;
	float maxHoldPositionX;
	Vector3 vec0;
	Vector3 v2;
	Rigidbody rigidbodyPlayer;
	bool started;
	PlayerState state;


	public enum PlayerState {
		Normal,
		Stretch,
		Jump,
		FallDown
	};


	void Start () {
		state = PlayerState.Normal;
		d = platform.GetComponent<PlatformTest>().WidthPeg1ToPeg2()/100;
		vec0 = platform.GetComponent<PlatformTest>().GetVec0();
		rigidbodyPlayer = GetComponent<Rigidbody>();
		started = true;
	}
	

	void Update () {
		MovingPlayer();



	}

	void PlayerMoving() {
		switch(state) {
			case PlayerState.Normal:
				
				break;
			case PlayerState.Stretch:
			
				break;
			case PlayerState.Jump:
			
				break;
			case PlayerState.FallDown:
			
				break;
		}
	}


	void FixedUpdate() {
		if (transform.position.y < platform.GetComponent<PlatformTest>().GetTopPeg1().y) {
			//rigidbodyPlayer.isKinematic = true; 
		}
	}


	void OnDrawGizmos() {
		if (started && transform.position.x < platform.GetComponent<PlatformTest>().GetTopPeg2().x
			&& transform.position.y - GetRadius() < platform.GetComponent<PlatformTest>().GetTopPeg2().y) {
			Vector3 desPos = new Vector3(transform.position.x, transform.position.y - transform.GetComponent<SphereCollider>().bounds.size.y/2, 
				transform.position.z);
			Gizmos.color = Color.green;
			Gizmos.DrawLine(platform.GetComponent<PlatformTest>().GetTopPeg1(), desPos);
			Gizmos.DrawLine(platform.GetComponent<PlatformTest>().GetTopPeg2(), desPos);
		}

//		if (started && transform.position.y - GetRadius() - platform.GetComponent<PlatformTest>().GetTopPeg2().y < 0.01f)
//		Gizmos.color = Color.green;
//		Gizmos.DrawLine(platform.GetComponent<PlatformTest>().GetTopPeg1(), platform.GetComponent<PlatformTest>().GetTopPeg2());
	}



	void Rotate(float angle) {
		transform.RotateAround(transform.position, Vector3.forward, angle);
	}


	void MovingPlayer() {
		if (!gamePlay.IsHold() && !gamePlay.startedHold) {
			if(x < d*100) {
				v2 = new Vector3(vec0.x + x , vec0.y + platform.GetComponent<PlatformTest>().GetCoefficientA() * (x  * x) + 
					platform.GetComponent<PlatformTest>().GetCoefficientB() * x + GetRadius(), platform.GetComponent<PlatformTest>().GetTopPeg1().z);
				transform.position = v2;
				x += d;
				Rotate(-4);
			}
		}
		else {
			transform.position = Vector3.Lerp(transform.position, new Vector3(GetPositionHoldMaxX(), 
				platform.GetComponent<PlatformTest>().GetPositionYWhenHoldMax(), transform.position.z), 0.03f);
			Rotate(-1);
		}

	}


	float GetRadius() {
		return GetComponent<SphereCollider>().bounds.size.x/2;
	}


	public void Jump() {
		//rigidbodyPlayer.useGravity = true;
		rigidbodyPlayer.AddForce(new Vector3(150, 500, 0)*1.4f);
	}


	public float GetPositionHoldMaxX() {
		return maxHoldPositionX;
	}

	public void SetPositionHoldMaxX(float max) {
		maxHoldPositionX = max;
	}





}
