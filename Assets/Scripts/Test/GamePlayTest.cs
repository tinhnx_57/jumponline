﻿using UnityEngine;
using System.Collections;

public class GamePlayTest : MonoBehaviour {
	public GameObject gameOver;
	bool hold;
	public Player3 player;
	float timeStartHold;
	static GamePlayTest gameplay;
	bool startFallOnLine;
	void Awake() {
		gameplay = this;
		Application.targetFrameRate = 60;
	}

	public static GamePlayTest current{
		get{
			return gameplay;
		}
	}

	void Start() {
		gameOver.SetActive(false);
	}

	void Update () {
		ControlHold();
	}

	void ControlHold() {
		if (Input.GetMouseButtonDown(0)) {
			hold = true;
		}
		if (Input.GetMouseButtonUp(0)) {
			hold = false;
		}
		if (hold && startFallOnLine) {
			timeStartHold = Time.time;
			SetPlayerWhenStretchLine();
			startFallOnLine = false;
		}
		if ((!hold && player.state == Player3.PlayerState.Stretch) || (timeStartHold != 0 && Time.time - timeStartHold > 1.0f && hold)) {
			SetPlayerJump();
			timeStartHold = 0;
		}

	}

	public bool IsHold() {
		return hold;
	}

	void SetPlayerWhenStretchLine() {
		player.SetPositionHoldMaxX(player.transform.position.x + 1f);
		player.SetState(Player3.PlayerState.Stretch);
		player.SetIsJumpNext(false); // in case user click when player is being in jump2
		PegPooler.getPooler.ChangeShowLine(false);
	}

	void SetPlayerJump() {
		player.SetVec0(player.transform.position);
		player.ResetX();
		player.CreateJumpParabol(Time.time - timeStartHold);
		player.SetState(Player3.PlayerState.Jump);
	}

	public void SetGameOver() {
		player.SetState(Player3.PlayerState.Die);
		gameOver.SetActive(true);
	}

	public void PlayGameAgain() {
		Application.LoadLevel("Test");
	}

	public void ChangeStartFallOnLineValue(bool fallOnLine) {
		startFallOnLine = fallOnLine;
	}
}
