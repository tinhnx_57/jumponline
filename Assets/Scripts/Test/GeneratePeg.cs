﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GeneratePeg : MonoBehaviour {
	public PoolObstacle obstaclePooler;
	public PegPooler pegPooler;
	public Player3 player;
	float distanceMin;
	float distanceMax;
	float rootY;
	float minY;
	float maxY;
	int pegAmount;



	void Start() {
		Init();
		rootY = transform.position.y;
		while(transform.position.x < player.transform.position.x + GameSettings.DISTANCE_TO_CREATE) {
			GameObject peg = pegPooler.GetPeg();
			peg.transform.position = transform.position;
			peg.SetActive(true);
//			obstaclePooler.CreateObstacle(peg);
			float randX = Random.Range(distanceMin, distanceMax);
			float randY = Random.Range(minY, maxY);
			transform.position = new Vector3(transform.position.x + randX, rootY + randY, 0);

		}
	}

	void Init() {
		distanceMin = GameSettings.DISTANCE_MIN;
		distanceMax = GameSettings.DISTANCE_MAX;
		minY = GameSettings.MIN_Y;
		maxY = GameSettings.MAX_Y;
		pegAmount = GameSettings.PEG_AMOUNT;
	}


	void Update () {
		if (transform.position.x < player.transform.position.x + GameSettings.DISTANCE_TO_CREATE) {
			GameObject peg = pegPooler.GetPeg();
			peg.transform.position = transform.position;
			peg.SetActive(true);
//			obstaclePooler.CreateObstacle(peg);
			float randX = Random.Range(distanceMin, distanceMax);
			float randY = Random.Range(minY, maxY);
			transform.position = new Vector3(transform.position.x + randX, rootY + randY, 0);

		}
	}
		

}
