﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PegPooler : MonoBehaviour {
	public GameObject pegPrefab;
	public Player3 player;
	List<GameObject> listPeg; //list peg avaiable
	List<GameObject> listPegTotal;
	static PegPooler pegPooler;

	float coefficientLineA; // coefficient line which player in, in ax + by + c = 0
	float coefficientLineB;
	float coefficientLineC;

	public float lineAfterJumpA {get; private set;} //coefficient a in state movingdirect when jumpdown near top peg 2
	public float lineAfterJumpB {get; private set;}

	public float parabolA {get; private set;}
	public float parabolB {get; private set;}
	public float alpha2Peg {get; private set;} //angle from player to peg 2, because we have "sai so :v"
	public float alphaTop1Top2 {get; private set;}


	public Vector3 top1 {get; private set;} //top peg 1
	public Vector3 top2 {get; private set;}
	bool canShowLine = true;
	float maxDistanceHoldY = 2f;


	void Awake() {
		pegPooler = this;
	}

	public static PegPooler getPooler {
		get {
			return pegPooler;
		}
	}

	void Start () {
		listPeg = new List<GameObject>();
		listPegTotal = new List<GameObject>();
	}

	void Update() {
		if (listPeg != null) {
			UpdateCoefficient();
			UpdatePlayerState();
			alphaTop1Top2 = Mathf.Atan2((top2.y - top1.y), (top2.x - player.transform.position.x));
		}
	}

	void UpdateCoefficient() { //update coefficient of line
		top1 = listPeg[PositionOfPlayerInList()].GetComponent<Peg>().GetTopPeg();
		top2 = listPeg[PositionOfPlayerInList() + 1].GetComponent<Peg>().GetTopPeg();
		coefficientLineA = top2.y - top1.y;
		coefficientLineB = top1.x - top2.x;
		coefficientLineC = - top1.x * top2.y + top2.x * top1.y;
	}

	void UpdatePlayerState()  { // update state when player moving throught line of top1 top2
		bool trueState = (player.state == Player3.PlayerState.Start || player.state == Player3.PlayerState.JumpDown || player.state == Player3.PlayerState.FallDown);
		if (trueState && RelativeWithLine(player.pointMeetLineCur) * RelativeWithLine(player.pointMeetLinePrev) <= 0) {
			GamePlayTest.current.ChangeStartFallOnLineValue(true);
			canShowLine = false;
			StartCoroutine(WaitToChangeState());
		}
		if ((player.state == Player3.PlayerState.Jump) && RelativeWithLine(player.pointMeetLineCur) 
			* RelativeWithLine(player.pointMeetLineNext) <= 0) {
			canShowLine = true; // destroy line match player and top1, top 2 (or if canShowLine == true, draw line from top1 to top2)
		}

	}


	IEnumerator WaitToChangeState() {
		if (player.isNearTopPeg2) {
			if (player.state == Player3.PlayerState.JumpDown && RelativeWithLine(player.GetTopParabolNext()) < 0 && player.widthJump > 3.0f) { // check distance top 1 to fix bug when player moving without click throught top peg
				CreateJumpNext();
			}
			else {
				ChangePlayerToFallOnLineState();
			}
		}
		else {
			yield return new WaitForSeconds(0);
			//game play seting can change
			//
			if (player.state == Player3.PlayerState.JumpDown && RelativeWithLine(player.GetTopParabolNext()) < 0 && player.widthJump > 3.0f) { // check distance top 1 to fix bug when player moving without click throught top peg
				CreateJumpNext();
			}
			else {
				ChangePlayerToFallOnLineState();
			}
		}
			
	}

	public void ChangePlayerToFallOnLineState() {
		player.SetIsJumpNext(false);
		canShowLine = false;
		alpha2Peg = - Mathf.Atan2((player.transform.position.y - player.radius - top2.y), (top2.x - player.transform.position.x));
		player.SetVec0(player.transform.position);
		player.ResetX();
		float width = Vector3.Distance(player.transform.position, top2 + new Vector3(0, player.radius, 0));
		if (width > player.radius * 3f) {
			float height = width / 10;
			parabolA = 4 * height / (width * width);
			parabolB = -4 * height / width;
			player.SetState(Player3.PlayerState.FallOnLine);
		}
		else {
			lineAfterJumpA = top2.y + player.radius - player.transform.position.y;
			lineAfterJumpB = player.transform.position.x - top2.x;
			player.SetState(Player3.PlayerState.MovingDirect); // fixed bug when player moving from right to left follow parabol
		}

	}



	public GameObject GetPeg() {
		GameObject newPeg = Instantiate(pegPrefab) as GameObject;
		newPeg.transform.parent = transform;
		newPeg.SetActive(false);
		listPeg.Add(newPeg);
		listPegTotal.Add(newPeg);
		return newPeg;
	}

	void OnDrawGizmos() {
		if (listPeg != null) {
			Gizmos.color = Color.green;
			for (int i = 0; i < listPeg.Count - 1; i++) {
				if (i != PositionOfPlayerInList()) {
					Vector3 fromPos = listPeg[i].GetComponent<Peg>().GetTopPeg();
					Vector3 toPos 	= listPeg[i+1].GetComponent<Peg>().GetTopPeg();
					Gizmos.DrawLine(fromPos, toPos);
				}
				else if (canShowLine) { // show line form top1 to top2
						Vector3 fromPos = listPeg[i].GetComponent<Peg>().GetTopPeg();
						Vector3 toPos 	= listPeg[i+1].GetComponent<Peg>().GetTopPeg();
						Gizmos.DrawLine(fromPos, toPos);
				}
				else { //show line form top1 to player and player to top2
					Vector3 fromPos = listPeg[i].GetComponent<Peg>().GetTopPeg();
					Vector3 toPos 	= listPeg[i+1].GetComponent<Peg>().GetTopPeg();
					Gizmos.DrawLine(fromPos, player.transform.position - new Vector3(0, player.radius, 0));
					Gizmos.DrawLine(player.transform.position - new Vector3(0, player.radius, 0), toPos);
				}
			}
		}

	}

	public int PositionOfPlayerInList() {
		for (int i = 0; i < listPeg.Count - 1; i++) {
			if (listPeg[i].GetComponent<Peg>().GetTopPeg().x < player.transform.position.x 
				&& listPeg[i+1].GetComponent<Peg>().GetTopPeg().x > player.transform.position.x) {
				return i;
			}
		}
		return 0;
	}

	public void RemoveFromList(GameObject obj) {
		listPeg.Remove(obj);
	}

	public List<GameObject> GetListPeg() {
		return listPeg;
	}

	public float RelativeWithLine(Vector3 vecCheck) { // return value < 0 if vecCheck is at left line and right line if another
		return coefficientLineA * vecCheck.x + coefficientLineB * vecCheck.y + coefficientLineC;
	}

	public float DistanceToLine(Vector3 fromPos) {
		Vector3 vecTopPeg1ToFromPos = fromPos - top1;
		Vector3 vecTopPeg2ToTopPeg1 = top2 - top1;
		float alphaBetween = Vector3.Angle(vecTopPeg1ToFromPos, vecTopPeg2ToTopPeg1) * Mathf.Deg2Rad; // angle of line and vector player - top peg1
		float distancePlayerTopPeg1 = Vector3.Distance(fromPos, top1);
		return distancePlayerTopPeg1 * Mathf.Sin(alphaBetween);
	}

	public float GetPositionYWhenHoldMax() {
		return CenterPointOfTopPeg1Peg2().y - maxDistanceHoldY;
	}

	public Vector3 CenterPointOfTopPeg1Peg2() {
		return new Vector3((top1.x + top2.x)/2, (top1.y + top2.y)/2, top1.z);
	}

	public void ChangeShowLine(bool change) {
		canShowLine = change;
	}

	public void AddPegToListPeg(GameObject newpeg) {
		listPeg.Add(newpeg);
	}


	void CreateJumpNext() { //jump without mouse click
		player.SetVec0(player.transform.position);
		player.ResetX();
		player.CreateJumpNextParabol();
		player.SetIsJumpNext(true);
		player.SetState(Player3.PlayerState.Jump);
	}


}
