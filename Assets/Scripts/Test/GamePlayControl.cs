﻿using UnityEngine;
using System.Collections;

public class GamePlayControl : MonoBehaviour {
	bool hold;
	public bool startedHold{ get; private set;}
	public MyPlayerTest player;
	Vector3 positionPlayerWhenMouseUp;
	void Awake() {
		
	}


	void Start () {
	
	}
	
	void Update () {
		ControlHold();
	}

	void ControlHold() {
		if (Input.GetMouseButtonDown(0)) {
			hold = true;
			startedHold = true;
			player.SetPositionHoldMaxX(player.transform.position.x + 1.2f);
		}
		else if (Input.GetMouseButtonUp(0)) {
			hold = false;
			Debug.Log("MouseUp!!!!");
			positionPlayerWhenMouseUp = transform.position;
			player.Jump();
		}
	}


	public bool IsHold() {
		return hold;
	}


}
