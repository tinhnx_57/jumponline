﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {
	bool canMoving = false;
	bool canScale = false;
	bool canRotate = true;
	float movingSpeed;
	float scaleSpeed;
	float maxDistanceMoving;
	float maxScale;
	float scaleY;
	float height;
	GameObject player;
	Vector3 startPos;
	Vector3 startScale;

	void OnEnable() {
		startPos = transform.position;
		scaleY = startScale.y;
	}

	void Start () {
		Init();
		player = GameObject.FindGameObjectWithTag("Hero");
		startPos = transform.position;
		startScale = transform.localScale;
		height = GetComponent<BoxCollider>().bounds.size.y;
		scaleY = transform.localScale.y;
	}

	void Init() {
		movingSpeed = GameSettings.OBSTACLE_MOVING_SPEED;
		scaleSpeed = GameSettings.OBSTACLE_SCALE_SPEED;
		maxDistanceMoving = GameSettings.OBSTACLE_MAX_DISTANCE_MOVING;
		maxScale = GameSettings.OBSTACLE_MAX_SCALE;
	}

		
	void Update () {
		CheckAndDestroy();
		if (canRotate) {
			Rotate(2);
		}
		if (canMoving) {
			Moving();
		}
		if (canScale) {
			Scale();
		}
	}

	void CheckAndDestroy() {
		if (transform.position.x < player.transform.position.x - GameSettings.DISTANCE_TO_DESTROY) {
			setObstacleWhenDeactive();
			gameObject.SetActive(false);
		}
	}

	void Rotate(float angle) {
		transform.RotateAround(transform.position, Vector3.up, angle);
	}

	void Moving() {
		if ((Vector3.Distance(transform.position, startPos) > maxDistanceMoving) || (transform.position.y < startPos.y)) {
			movingSpeed *= -1;
		}
		transform.Translate(0, movingSpeed * Time.deltaTime, 0);
	}

	void Scale() {
		if ((scaleY > maxScale) || (scaleY < startScale.y)) {
			scaleSpeed *= -1;
		}
		scaleY = transform.localScale.y + scaleSpeed * Time.deltaTime;
		transform.localScale = new Vector3(startScale.x, scaleY, startScale.z);
		Vector3 desPos = new Vector3(startPos.x, startPos.y + (scaleY - startScale.y)/2, startPos.z);
		transform.position = desPos;
	}
		
	public void SetMoving(bool canMov) {
		canMoving = canMov;
	}

	public void SetScale(bool canSc) {
		canScale = canSc;
	}

	public void SetRotate(bool rot) {
		canRotate = rot;
	}
	
	public void SetObstacleWhenGameOver() {
		canMoving = false;
		canRotate = false;
		canScale = false;
	}

	void setObstacleWhenDeactive() {
		movingSpeed = Mathf.Abs(movingSpeed);
		scaleY = Mathf.Abs(scaleY);
		canMoving = false;
		canScale = false;
		gameObject.transform.localScale = startScale;
	}

}
