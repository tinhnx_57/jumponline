﻿using UnityEngine;
using System.Collections;

public class Platform2 : MonoBehaviour {

	public GameObject peg1;
	public GameObject peg2;

	Vector3 vec0;

	// Use this for initialization
	void Start () {
		vec0 = GetTopPeg1();
		Debug.Log(WidthParabol());
		Debug.Log(GetTopPeg2().x - GetTopPeg1().x);
	}
	
	// Update is called once per frame
	void Update () {
		float deltaX = 0.1f;
		float x = 0;
		Vector3 v1 = vec0;
		Vector3 v2 = v1;
		float alpha = -Mathf.Atan2((GetTopPeg1().y - GetTopPeg2().y), (GetTopPeg2().x - GetTopPeg1().x)); //in radians
		while (x < WidthParabol()) {
			v2 = new Vector3(vec0.x + x, vec0.y + GetACofficientNormal() * x * x + GetBCofficientNormal() * x, peg1.transform.position.z);
			float xPos = (v2.x - vec0.x) * Mathf.Cos(alpha) - (v2.y - vec0.y) * Mathf.Sin(alpha) + vec0.x;
			float yPos = (v2.x - vec0.x) * Mathf.Sin(alpha) + (v2.y - vec0.y) * Mathf.Cos(alpha) + vec0.y;
			v2 = new Vector3(xPos, yPos, v2.z);
			Debug.DrawLine(v1, v2, Color.blue);
			v1 = v2;
			x += deltaX;
		}
	}



	public Vector3 GetTopPeg1() {
		return peg1.transform.position + new Vector3(0, peg1.GetComponent<BoxCollider>().bounds.size.y/2, 0);
	}

	public Vector3 GetTopPeg2() {
		return peg2.transform.position + new Vector3(0, peg2.GetComponent<BoxCollider>().bounds.size.y/2, 0);
	}


	void OnDrawGizmos() {
		Gizmos.color = Color.green;
		Gizmos.DrawLine(GetTopPeg1(), GetTopPeg2());
	}

	public Vector3 GetCenterTopPeg1Peg2() {
		return new Vector3((GetTopPeg1().x + GetTopPeg2().x)/2, (GetTopPeg1().y + GetTopPeg2().y)/2, peg1.transform.position.z);
	}

	float WidthParabol() {
		return Vector3.Distance(GetTopPeg1(), GetTopPeg2());
	}

	float HeightParabol() {
		return 1;
	}

	public float GetACofficientNormal() {
		return 4 * HeightParabol()/ (WidthParabol() * WidthParabol());
	}

	public float GetBCofficientNormal() {
		return -4 * HeightParabol() / WidthParabol();
	}

}
