﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PoolObstacle : MonoBehaviour {
	public GameObject obstacle;
	float obstacleMin = 6f; //distance 2 obstace in double obstacle
	float obstacleMax = 8.5f;
	int obstacleMount = 15;
	float heightObs;
	List<GameObject> listObstacle;


	void Start() {
		heightObs = obstacle.transform.localScale.y;
		listObstacle = new List<GameObject>();
		CreateList();
	}


	void CreateList() {
		for (int i = 0; i < obstacleMount; i++) {
			GameObject obs = (GameObject)Instantiate(obstacle);
			obs.transform.parent = transform;
			obs.SetActive(false);
			listObstacle.Add(obs);
		}
	}

	GameObject GetObstacle() {
		for (int i = 0; i < listObstacle.Count; i++) {
			if (!listObstacle[i].activeInHierarchy) {
				return listObstacle[i];
			}
		}
		GameObject obs = (GameObject)Instantiate(obstacle);
		obs.transform.parent = transform;
		obs.SetActive(false);
		listObstacle.Add(obs);
		return obs;
	}
		

	public void CreateObstacle(GameObject DesPos) {
		int rand = Random.Range(0, 2);
		if (rand == 1) {
			GameObject obs = GetObstacle();
			Vector3 des = DesPos.GetComponent<Peg>().GetTopPeg() + new Vector3(0, heightObs/2 , 0);
			obs.transform.position = des;
			obs.SetActive(true);
			int randShowObs2 = Random.Range(0, 2);
			if (randShowObs2 == 1) {
				float randObs2 = Random.Range(obstacleMin, obstacleMax);
				GameObject obs2 = GetObstacle();
				obs2.transform.parent = transform;
				Vector3 desObs2 = obs.transform.position + new Vector3(0, heightObs + randObs2, 0);
				obs2.transform.position = desObs2;
				obs2.SetActive(true);
			}

		}
		else {
			int randMoving = Random.Range(0, 5);
			if (randMoving == 1) {
				GameObject obs = GetObstacle();
				obs.transform.parent = transform;
				Vector3 des = DesPos.GetComponent<Peg>().GetTopPeg() + new Vector3(0, heightObs/2 , 0);
				obs.transform.position = des;
				obs.SetActive(true);
				obs.GetComponent<Obstacle>().SetMoving(true);
			}
			else if (randMoving == 2) {
				GameObject obs = GetObstacle();
				obs.transform.parent = transform;
				Vector3 des = DesPos.GetComponent<Peg>().GetTopPeg() + new Vector3(0, heightObs/2 , 0);
				obs.transform.position = des;
				obs.SetActive(true);
				obs.GetComponent<Obstacle>().SetScale(true);
			}

		}

	}


}
