﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerTest : MonoBehaviour {
	public float forceFactor = 1;

	Vector2 collisionPos;
	float mass;
	float force;
	float gravity;
	float alpha;
	Vector3 velocityPlayer;
	float velocityMax;
	Vector3 rotationPlayer;
	Vector3 playerForward;
	List<Vector3> listCollisionNormal;
	List<Vector3> listCollisionPoint;
	List<GameObject> listCollisionObject;
	bool canJump;

	Rigidbody2D playerRigid;


	void Start () {
		InitGame();
		velocityMax = 1f;
		listCollisionNormal = new List<Vector3>();
		listCollisionPoint = new List<Vector3>();
		listCollisionObject = new List<GameObject>();
	}

	void InitGame() {
		playerRigid = GetComponent<Rigidbody2D>();
		mass = playerRigid.mass;
		gravity = playerRigid.gravityScale;
	}

	void Update () {
		//		MovingPlayer();
		//velocityPlayer = GetComponent<Rigidbody2D>().velocity;
		//		alpha = -90 + Mathf.Atan2(velocityPlayer.y, velocityPlayer.x) * Mathf.Rad2Deg;
		//		Vector2 accelerator = gravity * Mathf.Cos(alpha);
		//		velocityBack += - accelerator * Time.deltaTime;
		//transform.position += -velocityPlayer * Time.deltaTime;
	}


	void MovingPlayer() {
		//transform.Translate(new Vector3(0.01f, 0, 0), Space.World);
		//		Debug.Log(Time.deltaTime);
		//		if (stay) {
		//			transform.position += velocityPlayer.normalized * Time.deltaTime;
		//		}
	}


	Vector3 GetVectorForward(Vector3 vec, Vector3 position) {
		return Vector3.zero;
	}


	int  GetIndex(List<Vector3> listVector) {  //get index of node right
		Vector3 tempPoint = listCollisionPoint[0];
		int index = new int();
		for (int i = 0; i < listVector.Count; i++) {
			if (listVector[i].x > tempPoint.x) {
				tempPoint = listVector[i];
				index = i;
			}
		}
		return index;
	}

	bool colliding = false;
	Vector2 desiredDirection;

	void OnCollisionEnter2D(Collision2D collision) {
		Vector3 normalPos = collision.contacts[0].normal;
		listCollisionNormal.Add(normalPos);
		listCollisionPoint.Add(collision.contacts[0].point);
		listCollisionObject.Add(collision.gameObject);

		colliding = true;
		//		playerRigid.isKinematic = true;
	}


	void OnCollisionStay2D(Collision2D collision) {
		if (collision.gameObject.tag == "Node") {
			canJump = true;
		}
		else if (collision.gameObject.tag == "Peg") {
			canJump = false;
		}

		GetComponent<Rigidbody2D>().velocity = Vector3.ClampMagnitude(GetComponent<Rigidbody2D>().velocity, velocityMax);
		velocityPlayer = GetComponent<Rigidbody2D>().velocity;

		Debug.DrawRay(transform.position, playerRigid.velocity*10, Color.green);
		Vector3 normalPos = collision.contacts[0].normal;
		Debug.DrawRay(collision.contacts[0].point, normalPos*-1, Color.red);
		Debug.DrawRay(collision.contacts[0].point, normalPos, Color.red);

		//transform.rotation = Quaternion.LookRotation(velocityPlayer);

		if (velocityPlayer.magnitude < 0.2f && velocityPlayer.x > 0) {
			Debug.Log("Start add force");
		}


		Vector3 vectorNormal = listCollisionNormal[GetIndex(listCollisionPoint)];
		Vector3 vecPt =  new Vector3(vectorNormal.y, -vectorNormal.x).normalized;
		Debug.DrawRay(transform.position, vecPt * 3, Color.black);

		desiredDirection = vecPt;

		transform.position += vecPt  * Time.deltaTime;
		//transform.position += new Vector3(- GetComponent<Rigidbody2D>().velocity.x, - GetComponent<Rigidbody2D>().velocity.y, 0)  * Time.deltaTime;


		//transform.position -= GetComponent<Rigidbody2D>().mass 
		//	* GetComponent<Rigidbody2D>().gravityScale * Mathf.Cos(Vector2.Angle(Vector2.down, vecPt)) * Vector3.down * Time.deltaTime;

		//		Debug.Log(Vector2.Angle(Vector2.down, vecPt));

		//phuong trinh phap tuyen: 
		// -vectorTotal.y * X + vectorTotal.x * Y + (vectorTotal.y * transform.position.x - vectorTotal.x * transform.position.y ) = 0


	}

	void FixedUpdate () {

		//		playerRigid.AddForce (-playerRigid.mass * playerRigid.velocity, ForceMode2D.Impulse);

		//		playerRigid.AddForce (-playerRigid.mass * playerRigid.gravityScale * Physics.gravity);
		if (colliding) {
			//			playerRigid.velocity = Vector2.zero;
			//			playerRigid.AddForce(-playerRigid.mass * playerRigid.velocity / Time.fixedDeltaTime);


			//			playerRigid.AddForce (-playerRigid.mass * playerRigid.velocity, ForceMode2D.Impulse);
			//			playerRigid.AddForce(desiredDirection * playerRigid.mass * forceFactor);

			//			playerRigid.velocity = desiredDirection * forceFactor;
			//			var desiredVelocity = desiredDirection * forceFactor;
			//			playerRigid.AddForce(playerRigid.mass * (desiredVelocity - playerRigid.velocity), ForceMode2D.Impulse);

			//			playerRigid.AddTorque (-playerRigid.inertia * playerRigid.angularVelocity, ForceMode2D.Impulse );

			//			playerRigid.AddTorque (-playerRigid.inertia * forceFactor);

			//			playerRigid.angularVelocity = forceFactor;
		}

	}

	void OnCollisionExit2D(Collision2D collision) {
		for (int i = 0; i < listCollisionObject.Count; i++) {
			if (listCollisionObject[i] == collision.gameObject) {
				listCollisionObject.Remove(listCollisionObject[i]);
				listCollisionNormal.Remove(listCollisionNormal[i]);
				listCollisionPoint.Remove(listCollisionPoint[i]);
			}
		}

		colliding = false;
		//		playerRigid.isKinematic = false;
	}


	public void Jump() {
		if (canJump) {
			GetComponent<Rigidbody2D>().AddForce(new Vector3(2, 7, 0) * (GetComponent<Rigidbody2D>().mass + 20));
			GetComponent<Rigidbody2D>().mass = 1;
			canJump = false;
		}

	}
}
