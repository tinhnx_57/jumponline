﻿using UnityEngine;
using System.Collections;

public class PegLink : MonoBehaviour {
	public GameObject player;
	float distanceToPlayer = 40;

	void Update () {
		FollowPlayer();
	}

	void FollowPlayer() {
		transform.position = new Vector3(player.transform.position.x + distanceToPlayer, transform.position.y, transform.position.z);
	}
}
