﻿using UnityEngine;
using System.Collections;

public class PlatformTest : MonoBehaviour {
	public GameObject peg1;
	public GameObject peg2;

	float heightPeg1;
	float heightPeg2;
	float maxDistanceHoldY;
	Vector3[] listPointParabol;
	Vector3 vec0;

	void Awake() {
		heightPeg1 = peg1.GetComponent<BoxCollider>().bounds.size.y/2;
		heightPeg2 = peg2.GetComponent<BoxCollider>().bounds.size.y/2;
	}

	void Start () {
		vec0 = GetTopPeg1();
		maxDistanceHoldY = 1.2f;
	}

	void Update() {
		DrawParabol();
	}


	void DrawParabol() {
		float x = 0;
		float d = WidthPeg1ToPeg2()/50;
		Vector3 v1 = vec0;
		Vector3 v2 = v1;
		while(x < WidthPeg1ToPeg2()) {
			v2 = new Vector3(vec0.x + x , vec0.y + GetCoefficientA() * (x  * x) + GetCoefficientB() * x , peg1.transform.position.z);
			Debug.DrawLine(v1, v2, Color.blue);
			v1 = v2;
			x += d;
		}
	}


	void OnDrawGizmos() {
		Gizmos.color = Color.green;
		Gizmos.DrawLine(GetTopPeg1(), GetTopPeg2());
	}

	public Vector3 GetTopPeg1() {
		return new Vector3(peg1.transform.position.x, peg1.transform.position.y + heightPeg1, transform.position.z);
	}

	public Vector3 GetTopPeg2() {
		return new Vector3(peg2.transform.position.x, peg2.transform.position.y + heightPeg2, transform.position.z);
	}

	public float GetHeightPeg1() {
		return heightPeg1;
	}

	public float GetHeightPeg2() {
		return heightPeg2;
	}
		
	public float WidthPeg1ToPeg2() {
		return Vector3.Distance(GetTopPeg1(), GetTopPeg2());
	}

	float HeightParabol() {
		Debug.DrawLine(CenterPointOfTopPeg1Peg2(), GetVertexParabol());
		return Vector3.Distance(CenterPointOfTopPeg1Peg2(), GetVertexParabol());
	}


	public Vector3 CenterPointOfTopPeg1Peg2() {
		return new Vector3((GetTopPeg1().x + GetTopPeg2().x)/2, (GetTopPeg1().y + GetTopPeg2().y)/2, peg1.transform.position.z);
	}


	Vector3 GetVertexParabol() {
		Vector3 vertexParabol = CenterPointOfTopPeg1Peg2() - new Vector3(0, 0.5f, 0);
		return vertexParabol;
	}

	public float GetCoefficientA() { //coefficient a in y= ax^2 + bx + c
		return 4*HeightParabol() / (WidthPeg1ToPeg2() * WidthPeg1ToPeg2());
	}

	public float GetCoefficientB() { //coefficient b in y= ax^2 + bx + c
		return -4*HeightParabol() / WidthPeg1ToPeg2();
	}

	public Vector3 GetVec0() {
		return GetTopPeg1();
	}


	public float GetPositionYWhenHoldMax() {
		return CenterPointOfTopPeg1Peg2().y - maxDistanceHoldY;
	}




}

