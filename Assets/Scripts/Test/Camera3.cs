﻿using UnityEngine;
using System.Collections;

public class Camera3 : MonoBehaviour {

	public Player3 player;
	float z_camera;
	float y_camera;
	float x_camera;

	void Start() {
		z_camera = GameSettings.CAMERA_Z;
		y_camera = GameSettings.CAMERA_Y;
		x_camera = GameSettings.CAMERA_X;
	}


	void LateUpdate () {
		CameraFollowPlayer();
	}


	void CameraFollowPlayer() {
		Vector3 lerpPos = new Vector3(player.transform.position.x + x_camera, y_camera, z_camera);
//		transform.position = Vector3.Lerp(transform.position, lerpPos, 10.0f * Time.deltaTime);
		transform.position = lerpPos;
	}
}
