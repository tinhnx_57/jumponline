﻿using UnityEngine;
using System.Collections;

public class StretchLine : MonoBehaviour {
	public GameObject meshLine;
	public GameObject player;

	GameObject[] meshObject;
	Mesh[] mesh;
	Vector3 fromPos;
	Vector3 toPos;

	float[] xPosition;
	float[] yPosition;
	float bottom;
	float z = -1f;

	void Start () {
		fromPos = transform.GetComponent<ControlLines>().GetFromPos();
		toPos = transform.GetComponent<ControlLines>().GetToPos();
		//SpawnLine(fromPos.z, Vector3.Distance(fromPos, toPos), fromPos.y, fromPos.y - 5f);
	}
	

	void Update () {
	
	}


	void SpawnLine(float left, float width, float top, float bot) {
		int edgeCount = Mathf.RoundToInt(width) * 2;
		int vertexCount = edgeCount + 1;

		meshObject = new GameObject[edgeCount];
		mesh = new Mesh[edgeCount];

		xPosition = new float[vertexCount];
		yPosition = new float[vertexCount];
		bottom = bot;

		for (int i = 0; i < vertexCount; i++) {
			xPosition[i] = left + (i * width)/vertexCount;
			yPosition[i] = top;
		}

		for (int i = 0; i < edgeCount; i++) {
			mesh[i] = new Mesh();

			Vector3[] vertex = new Vector3[4];
			vertex[0] = new Vector3(xPosition[i], yPosition[i], z);
			vertex[1] = new Vector3(xPosition[i+1], yPosition[i+1], z);
			vertex[2] = new Vector3(xPosition[i], bottom, z);
			vertex[3] = new Vector3(xPosition[i+1], bottom, z);


			Vector2[] UV  = new Vector2[4];
			UV[0] = new Vector2(0, 0);
			UV[1] = new Vector2(1, 0);
			UV[2] = new Vector2(1, 1);
			UV[3] = new Vector2(0, 1);

			int[] tris = new int[6] {0, 1, 2, 2, 1, 3};

			mesh[i].vertices = vertex;
			mesh[i].uv = UV;
			mesh[i].triangles = tris;

			meshObject[i] = Instantiate(meshLine, transform.position, Quaternion.identity) as GameObject;
			meshObject[i].transform.parent = transform;
			meshObject[i].GetComponent<MeshFilter>().mesh = mesh[i];
		}

	}


	void UpdateMeshs() {
		for (int i = 0; i < mesh.Length; i++) {
			Vector3[] vertex = new Vector3[4];
			vertex[0] = new Vector3(xPosition[i], yPosition[i], z);
			vertex[1] = new Vector3(xPosition[i + 1], yPosition[i + 1], z);
			vertex[2] = new Vector3(xPosition[i], bottom, z);
			vertex[3] = new Vector3(xPosition[i + 1], bottom, z);

			mesh[i].vertices = vertex;
		}
	}

}
