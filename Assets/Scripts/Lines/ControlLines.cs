﻿using UnityEngine;
using System.Collections;

public class ControlLines : MonoBehaviour {
	public GameObject player;
	public Transform fromPos;
	public Transform toPos;

	private LineRenderer lineRender;


	void Start () {
		lineRender = GetComponent<LineRenderer>();
		lineRender.SetVertexCount(3);
		lineRender.SetWidth(0.1f, 0.1f);
		lineRender.SetPosition(0, fromPos.position);
		lineRender.SetPosition(1, player.transform.position);
		lineRender.SetPosition(2, toPos.position);


	}

	void Update() {
		UpdateLineThroughtPlayer();
	}

	void UpdateLineThroughtPlayer() {
		lineRender.SetPosition(0, fromPos.position);
		lineRender.SetPosition(1, player.transform.position);
		lineRender.SetPosition(2, toPos.position);
	}

	public Vector3 GetFromPos() {
		return fromPos.position;
	}

	public Vector3 GetToPos() {
		return toPos.position;
	}

}
