﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class PlatformPooler : MonoBehaviour {
	public GameObject[] listPlatformPrefabs;
	int platformAmount = 10;
	List<GameObject> listPlatform;

	void Start () {
		listPlatform = new List<GameObject>();
		for (int i = 0; i < platformAmount; i++) {
			int rand = Random.Range(0, listPlatformPrefabs.Length);
			GameObject obj = (GameObject)Instantiate(listPlatformPrefabs[rand], transform.position, Quaternion.identity);
			obj.transform.parent = transform;
			obj.SetActive(false);
			listPlatform.Add(obj);
		}
	}

	public GameObject GetPlatform() {
		for (int i = 0; i < listPlatform.Count; i++) {
			if (!listPlatform[i].activeInHierarchy) {
				return listPlatform[i];
			}
		}
		int rand = Random.Range(0, listPlatformPrefabs.Length);
		GameObject newPlatform = (GameObject)Instantiate(listPlatformPrefabs[rand]);
		newPlatform.transform.parent = transform;
		newPlatform.SetActive(false);
		listPlatform.Add(newPlatform);
		return newPlatform;
	}


}
