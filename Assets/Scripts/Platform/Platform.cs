﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour {
	public GameObject peg1;
	public GameObject peg2;
	public bool canShowLine{get; private set;}
	float heightPeg1;
	float heightPeg2;
	float widthPeg;
	float widthFlatform;
	float maxDistanceHoldY;
	float alpha; //angle of top peg1 and top peg2 in radian
	float alphaBetween; // between player, top peg1, line
	float angleWithIdentity; //angle of line and vec3.up
	float coefficientALine; //ax + by + c = 0 line of top peg1 and top peg2
	float coefficientBLine;
	float coefficientCLine;
	Vector3[] listPointParabol;
	Vector3 vec0;
	GameObject deActivePoint;
	Player player;
	bool playerInPlatform;

	void Awake() {
		canShowLine = true;
		player = GameObject.FindGameObjectWithTag("Hero").GetComponent<Player>();
		heightPeg1 = peg1.GetComponent<BoxCollider>().bounds.size.y/2;
		heightPeg2 = peg2.GetComponent<BoxCollider>().bounds.size.y/2;

	}

	void Start () {
		widthPeg = peg1.GetComponent<BoxCollider>().bounds.size.x/2;
		widthFlatform = Vector2.Distance(peg1.transform.position, peg2.transform.position) + widthPeg*2;
		deActivePoint = GameObject.FindGameObjectWithTag("DeActivePoint");
		vec0 = GetTopPeg1();
		maxDistanceHoldY = 1.5f;
		alpha = -Mathf.Atan2((GetTopPeg1().y - GetTopPeg2().y), (GetTopPeg2().x - GetTopPeg1().x)); //in radians
	}

	void OnEnable() {
		CalculateCoefficient();
	}


	void CalculateCoefficient() {
		coefficientALine = GetTopPeg2().y - GetTopPeg1().y;
		coefficientBLine = GetTopPeg1().x - GetTopPeg2().x;
		coefficientCLine = - GetTopPeg1().x * GetTopPeg2().y + GetTopPeg2().x * GetTopPeg1().y;
	}

	public float RelativePositionWithLine(Vector3 position) {
		return coefficientALine * position.x + coefficientBLine * position.y + coefficientCLine;
	}

	void Update () {
		ControlDeactiveThis();
		CheckMetPlayer();
		CheckPlayerDie();
		UpdatePlayerState();
		ControlShowLine();
	}
		
	void ControlDeactiveThis() {
		if (transform.position.x < deActivePoint.transform.position.x) {
			gameObject.SetActive(false);
		}
	}

	void CheckMetPlayer() { //check player moving between peg1, peg2
		if (!playerInPlatform && player.transform.position.x > peg1.transform.position.x && player.transform.position.x < peg2.transform.position.x) {
			playerInPlatform = true;
			player.SetPlatform(this);
		}
		if (playerInPlatform && (player.transform.position.x < peg1.transform.position.x || player.transform.position.x > peg2.transform.position.x)) {
			playerInPlatform = false;
//			print("Out of this platform!!!");
		}

	}

	void CheckPlayerDie() {
		if (playerInPlatform && player.transform.position.y < (peg1.transform.position.y - peg1.GetComponent<BoxCollider>().bounds.size.y/2)) {
			GamePlay.current.SetGameOver();
		}
	}

	void ControlShowLine() {
		if ((RelativePositionWithLine(player.transform.position - new Vector3(0, player.GetRadius(), 0))
			* RelativePositionWithLine(player.GetNextBottomPosition()) <= 0) 
			&& (player.state == Player.PlayerState.Jump || player.state == Player.PlayerState.FallDown || player.state == Player.PlayerState.Start)) {
			canShowLine = true;
		}
	}
		
	void UpdatePlayerState() {
		bool trueState = (player.state == Player.PlayerState.Start 
			|| player.state == Player.PlayerState.JumpDown || player.state == Player.PlayerState.FallDown);
		if (playerInPlatform && trueState 
			&& (RelativePositionWithLine(player.transform.position - new Vector3(0, player.GetRadius(), 0)) 
				* RelativePositionWithLine(player.GetPreviousBottom()) <= 0)) {
			float alphaWithTopPeg2 = Mathf.Atan2((player.transform.position.y - GetTopPeg2().y - player.GetRadius()), 
				(GetTopPeg2().x - player.transform.position.x));
			float widthParabol = Vector3.Distance(player.transform.position, GetTopPeg2() + new Vector3(0, player.GetRadius(), 0));
			float heightParabol = widthParabol/10;
			float coefAFallOnLine = 4 * heightParabol / (widthParabol * widthParabol);
			float coefBFallOnLine = -4 * heightParabol / widthParabol;
			player.SetAlphaWhenStartMetLine(-alphaWithTopPeg2);
			player.SetCoefficientFallOnLine(coefAFallOnLine, coefBFallOnLine);
			player.SetVec0(player.transform.position);
			player.ResetX();
			player.SetState(Player.PlayerState.FallOnLine);
			canShowLine = false;
			GamePlay.current.ChangeStartFallOnLineValue(true);
		}


	}


	public float DistanceToLine(Vector3 fromPos) {
		Vector3 vecTopPeg1ToFromPos = fromPos - GetTopPeg1();
		Vector3 vecTopPeg2ToTopPeg1 = GetTopPeg2() - GetTopPeg1();
		alphaBetween = Vector3.Angle(vecTopPeg1ToFromPos, vecTopPeg2ToTopPeg1) * Mathf.Deg2Rad; // angle of line and vector player - top peg1
		float distancePlayerTopPeg1 = Vector3.Distance(fromPos, GetTopPeg1());
		return distancePlayerTopPeg1 * Mathf.Sin(alphaBetween);
	}


	public float GetWidthFlatform() {
		return widthFlatform;
	}
		
	void OnDrawGizmos() {
		if (canShowLine) {
			Gizmos.color = Color.green;
			Gizmos.DrawLine(GetTopPeg1(), GetTopPeg2());
		}
		else if (!canShowLine && playerInPlatform 
			&& (player.transform.position.y > (peg1.transform.position.y - peg1.GetComponent<BoxCollider>().bounds.size.y/2))) {
			Vector3 desPos = new Vector3(player.transform.position.x, player.transform.position.y - player.GetComponent<SphereCollider>().bounds.size.y/2, 
				player.transform.position.z);
			Gizmos.color = Color.green;
			Gizmos.DrawLine(GetTopPeg1(), desPos);
			Gizmos.DrawLine(GetTopPeg2(), desPos);
		}
	}

//	void DrawParabol() {
//		float x = 0;
//		float d = WidthPeg1ToPeg2()/50;
//		Vector3 v1 = vec0;
//		Vector3 v2 = v1;
//		while (x < WidthPeg1ToPeg2()) {
//			v2 = new Vector3(vec0.x + x , vec0.y + GetCoefficientA() * (x  * x) + GetCoefficientB() * x , peg1.transform.position.z);
//			float xPos = (v2.x - vec0.x) * Mathf.Cos(alpha) - (v2.y - vec0.y) * Mathf.Sin(alpha) + vec0.x;
//			float yPos = (v2.x - vec0.x) * Mathf.Sin(alpha) + (v2.y - vec0.y) * Mathf.Cos(alpha) + vec0.y;
//			v2 = new Vector3(xPos, yPos, v2.z);
//			Debug.DrawLine(v1, v2, Color.blue);
//			v1 = v2;
//			x += d;
//		}
//	}

	public void SetShowLine(bool canshow) {
		canShowLine = canshow;
	}
		
	public Vector3 GetTopPeg1() {
		return new Vector3(peg1.transform.position.x, peg1.transform.position.y + heightPeg1, transform.position.z);
	}

	public Vector3 GetTopPeg2() {
		return new Vector3(peg2.transform.position.x, peg2.transform.position.y + heightPeg2, transform.position.z);
	}

	public float GetHeightPeg1() {
		return heightPeg1;
	}

	public float GetHeightPeg2() {
		return heightPeg2;
	}

	public float WidthPeg1ToPeg2() {
		return Vector3.Distance(GetTopPeg1(), GetTopPeg2());
	}

	float HeightParabol() {
		return Vector3.Distance(CenterPointOfTopPeg1Peg2(), GetVertexParabol());
	}
		
	public Vector3 CenterPointOfTopPeg1Peg2() {
		return new Vector3((GetTopPeg1().x + GetTopPeg2().x)/2, (GetTopPeg1().y + GetTopPeg2().y)/2, peg1.transform.position.z);
	}


	Vector3 GetVertexParabol() {
		Vector3 vertexParabol = CenterPointOfTopPeg1Peg2() - new Vector3(0, 0.8f, 0);
		return vertexParabol;
	}

	public float GetCoefficientA() { //coefficient a in y= ax^2 + bx + c
		return 4*HeightParabol() / (WidthPeg1ToPeg2() * WidthPeg1ToPeg2());
	}

	public float GetCoefficientB() { //coefficient b in y= ax^2 + bx + c
		return -4*HeightParabol() / WidthPeg1ToPeg2();
	}

	public Vector3 GetVec0() {
		return GetTopPeg1();
	}

	public float GetPositionYWhenHoldMax() {
		return CenterPointOfTopPeg1Peg2().y - maxDistanceHoldY;
	}

	public void ChangeShowLine(bool show) {
		canShowLine = show;
	}

	public float GetAlpha() {
		return alpha;
	}


}
