﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GeneratePlatform : MonoBehaviour {
	public Transform generatePoint;
	public GameObject platformPrefab;
	float distanceMinX = 8.5f;
	float distanceMaxX = 10;

	float distanceMinY = 1.5f;
	float distanceMaxY = 4f;
	public PlatformPooler platformPooler;



	void Update () {
		if (transform.position.x < generatePoint.position.x) {
			GameObject platform = platformPooler.GetPlatform();
			platform.transform.position = transform.position;
			platform.SetActive(true);
			float randX = Random.Range(distanceMinX, distanceMaxX);
			float randY = Random.Range(distanceMinY, distanceMaxY);
			transform.position = new Vector3(transform.position.x + randX, transform.position.y , transform.position.z);
		}
	}
		


}
