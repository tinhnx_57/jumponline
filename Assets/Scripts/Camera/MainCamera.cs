﻿using UnityEngine;
using System.Collections;

public class MainCamera : MonoBehaviour {
	public Player player;


	void Update () {
		CameraFollowPlayer();
	}

	void CameraFollowPlayer() {
		Vector3 lerpPos = new Vector3(player.transform.position.x + 3, player.transform.position.y, -10);
		transform.position = Vector3.Lerp(transform.position, lerpPos, 0.15f);
	}

}
