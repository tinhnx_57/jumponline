﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Player : MonoBehaviour {
	public GamePlay gamePlay;
	float x = 0;
	float deltaX;
	float deltaXJump;
	float maxHoldPositionX;
	float coefAFallOnLine; //coefficient a of parabol y = ax^2 + bx in fallOnLine state
	float coefBFallOnLine;
	float yNextPosition;
	float aFallOnLine; //cofficient a in : y = ax
	float alphaWhenStartFallOnLine; //in radian, (tolerance when trigger with top peg 2)

	Vector3 vec0; //vector root when moving follow parabol
	Vector3 v2;
	Vector3 desPointFallOnLine; //destination point in state fallOnLine
	Vector3 nextBottomPosition;
	Vector3 previousBottomPosition;
	Rigidbody rigidbodyPlayer;
	Platform platform;
	public PlayerState state {get; private set;}

	const float  heightFall = 2f;
	const float widthFall = 4f;

	float heighJump;
	float widthJump;
	public bool playernearTopPeg{get; private set;}

	public enum PlayerState {
		Start,
		Stretch,
		Jump,
		JumpDown,
		FallOnLine,
		FallDown,
		Die
	};


	void Start () {
		state = PlayerState.Start;
		deltaX = 0.065f;
		rigidbodyPlayer = GetComponent<Rigidbody>();
		vec0 = transform.position;
	}

	void Update () {
		if (platform != null) {
			UpdatePlayerState();
			PlayerMoving();
			CheckPlayerNearTopPeg2();
		}
	}

	void CheckPlayerNearTopPeg2() {
		if (Vector3.Distance(transform.position, platform.GetTopPeg2()) < 2 * GetRadius()) {
			playernearTopPeg = true;
		}
		else {
			playernearTopPeg = false;
		}

	}
		
	void PlayerMoving() {
		float alpha = platform.GetAlpha();
		switch(state) {
			case PlayerState.Start:
				previousBottomPosition = transform.position - new Vector3(0, GetRadius(), 0);
				transform.position -= new Vector3(0, 0.15f, 0);
				yNextPosition = transform.position.y - 0.15f;
				nextBottomPosition = transform.position - new Vector3(0, 0.15f, 0) - new Vector3(0, GetRadius(), 0);
				Rotate(-4);
				break;
			case PlayerState.Stretch:
				transform.position = Vector3.Lerp(transform.position, new Vector3(GetPositionHoldMaxX(), 
				platform.GetPositionYWhenHoldMax(), transform.position.z), 0.09f);
				Rotate(-4);
				break;
			case PlayerState.Jump:
				previousBottomPosition = transform.position - new Vector3(0, GetRadius(), 0);
				v2 = new Vector3(vec0.x + x, vec0.y + GetAJump() * (x * x) + GetBJump() * x, transform.position.z);
				transform.position = v2;
				x += deltaX * 3.2f;
				yNextPosition = vec0.y + GetAJump() * (x * x) + GetBJump() * x;
				nextBottomPosition = new Vector3(vec0.x + x, vec0.y + GetAJump() * (x * x) + GetBJump() * x, transform.position.z) - new Vector3(0, GetRadius(), 0);;
				if (x > widthJump/2) {
					state = PlayerState.JumpDown;
				}
				Rotate(-4);
				break;
			case PlayerState.JumpDown:
				previousBottomPosition = transform.position - new Vector3(0, GetRadius(), 0);
				v2 = new Vector3(vec0.x + x, vec0.y + GetAJump() * (x * x) + GetBJump() * x, transform.position.z);
				transform.position = v2;
				x += deltaX * 3.2f;
				yNextPosition = vec0.y + GetAJump() * (x * x) + GetBJump() * x;
				nextBottomPosition = new Vector3(vec0.x + x, vec0.y + GetAJump() * (x * x) + GetBJump() * x, transform.position.z) - new Vector3(0, GetRadius(), 0);;
				Rotate(-4);
				break;
			case PlayerState.FallOnLine:
				v2 = new Vector3(vec0.x  + x, vec0.y + coefAFallOnLine * x * x + coefBFallOnLine * x, transform.position.z);
				float xPosFallOnLine = (v2.x - vec0.x) * Mathf.Cos(alphaWhenStartFallOnLine) - (v2.y - vec0.y) * Mathf.Sin(alphaWhenStartFallOnLine) + vec0.x;
				float yPosFallOnLine = (v2.x - vec0.x) * Mathf.Sin(alphaWhenStartFallOnLine) + (v2.y - vec0.y) * Mathf.Cos(alphaWhenStartFallOnLine) + vec0.y;
				transform.position = new Vector3(xPosFallOnLine, yPosFallOnLine, transform.position.z);
				x += deltaX * 1.7f;
				Rotate(-4);
				break;
			case PlayerState.FallDown:
				previousBottomPosition = transform.position - new Vector3(0, GetRadius(), 0);
				v2 = new Vector3(vec0.x + x, vec0.y + GetAFall() * (x * x) + GetBFall() * x, transform.position.z);
				transform.position = v2;
				x += deltaX * 2f;
				yNextPosition = vec0.y + GetAFall() * (x * x) + GetBFall() * x;
				nextBottomPosition = new Vector3(vec0.x + x, vec0.y + GetAFall() * (x * x) + GetBFall() * x, transform.position.z) - new Vector3(0, GetRadius(), 0);;
				Rotate(-4);
				break;
		}
	}
		
	void OnTriggerEnter(Collider col) {
		if (col.tag == "Peg1") {
			if (transform.position.x < col.transform.position.x) {
				gamePlay.SetGameOver();
			}
		}
		else if (col.tag == "Peg2") {
			if (transform.position.y - GetRadius() < platform.GetTopPeg2().y && state == PlayerState.Jump) {
				gamePlay.SetGameOver();
			}
		}
	}

	public void SetCoefficientFallOnLine(float coefA, float coefB) {
		coefAFallOnLine = coefA;
		coefBFallOnLine = coefB;
	}

	public void SetAlphaWhenStartMetLine(float angle) {
		alphaWhenStartFallOnLine = angle;
	}

	void OnTriggerExit(Collider col) {
		if (col.tag == "Line" && (state == PlayerState.FallOnLine || state == PlayerState.Jump)) {
			platform.SetShowLine(true);
		}
		if (col.tag == "Peg2") {
			//playernearTopPeg2 = false;
		}
	}

	void UpdatePlayerState() {
		if (platform != null) {
			if (transform.position.x > platform.GetTopPeg2().x && state == PlayerState.FallOnLine) {
				state = PlayerState.FallDown;
				vec0 = new Vector3(transform.position.x - widthFall/2, transform.position.y - heightFall, transform.position.z);
				x = widthFall/2;
				platform.GetComponent<Platform>().ChangeShowLine(true);
				gamePlay.ChangeStartFallOnLineValue(false);
			}
		}
	}

	public Vector3 GetPreviousBottom() {
		return previousBottomPosition;
	}

	public float GetYNextPosition() {
		return yNextPosition;
	}

	public Vector3 GetNextBottomPosition() {
		return nextBottomPosition;
	}
		
	void Rotate(float angle) {
		transform.RotateAround(transform.position, Vector3.forward, angle);
	}
		
	public float GetRadius() {
		return GetComponent<SphereCollider>().bounds.size.x/2;
	}
		
	public void CreateJumpParabol() {
		float distanceToLine = platform.DistanceToLine(transform.position - new Vector3(0, GetRadius(), 0));
		float angle = 90 - Mathf.Abs(platform.GetAlpha() * Mathf.Rad2Deg); //in radian
		float distanceFollowVec3Up = distanceToLine * Mathf.Sin(angle * Mathf.Deg2Rad);
		heighJump = 4.2f * distanceFollowVec3Up;
		widthJump = 5.8f * distanceFollowVec3Up;
		//deltaXJump = widthJump/26;
	}

	public float GetPositionHoldMaxX() {
		return maxHoldPositionX;
	}

	public void SetPositionHoldMaxX(float max) {
		maxHoldPositionX = max;
	}
		
	public void SetState (PlayerState st) {
		state = st;
	}

	float GetAFall() {
		return -4 * heightFall/ (widthFall * widthFall);
	}

	float GetBFall() {
		return 4 * heightFall / widthFall;
	}

	float GetAJump() {
		return -4 * heighJump / (widthJump * widthJump);
	}

	float GetBJump() {
		return 4 * heighJump / widthJump;
	}


	public void SetVec0(Vector3 pos) {
		vec0 = pos;
	}

	public void ResetX() {
		x = 0;
	}

	public void SetPlatform(Platform plp) {
		platform = plp;
	}
		
	public void SetAFallOnLine(float a) {
		aFallOnLine = a;
	}

	float GetAFallOnLine() {
		return aFallOnLine;
	}
		

	public void SetDesPointInFallOnLineState(Vector3 des) {
		desPointFallOnLine = des;
	}
		

	public void ChangeDeltaXJump(float delta) {
		deltaXJump = delta;
	}


}
