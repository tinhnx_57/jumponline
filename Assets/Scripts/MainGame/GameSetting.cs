﻿using UnityEngine;
using System.Collections;

public static class GameSettings{

	public static float PLAYER_MOVING_SPEED = 3.6f;

	//distance from player
	public static float DISTANCE_TO_DESTROY = 40f;
	public static float DISTANCE_TO_CREATE = 120f;
	public static float DELTA_X = 0.2f;

	public static float WIDTH_FALL = 5f;
	public static float HEIGHT_FALL = 6f;

	public static float CAMERA_Z = -16.0f;
	public static float CAMERA_Y = 15.0f;
	public static float CAMERA_X = - 18.0f; // with player

	//generate peg
	public static float DISTANCE_MIN = 20.0f;
	public static float DISTANCE_MAX = 25.0f;
	public static float MIN_Y = -2.0f;
	public static float MAX_Y = 2.0f;
	public static int PEG_AMOUNT = 8;


	//obstacle
	public static float OBSTACLE_MOVING_SPEED = 6.0f;
	public static float OBSTACLE_SCALE_SPEED = 2.0f;
	public static float OBSTACLE_MAX_DISTANCE_MOVING = 8.0f;
	public static float OBSTACLE_MAX_SCALE = 5.0f;

}
