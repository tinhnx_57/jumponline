﻿using UnityEngine;
using System.Collections;

public class GamePlay : MonoBehaviour {
	public Canvas gameOverCanvas;
	bool hold;
	public Player player;
	float timeStartHold;
	static GamePlay gameplay;
	bool startFallOnLine;
	void Awake() {
		gameplay = this;
	}

	public static GamePlay current{
		get{
			return gameplay;
		}
	}

	void Start() {
		gameOverCanvas.gameObject.SetActive(false);
	}

	void Update () {
		ControlHold();
	}

	void ControlHold() {
		if (Input.GetMouseButtonDown(0)) {
			hold = true;
		}
		if (Input.GetMouseButtonUp(0)) {
			hold = false;
		}
		if (hold && startFallOnLine && !player.playernearTopPeg) {
			timeStartHold = Time.time;
			SetPlayerWhenStretchLine();
			startFallOnLine = false;
		}
		if ((!hold && player.state == Player.PlayerState.Stretch) || (timeStartHold != 0 && Time.time - timeStartHold > 1.0f && hold)) {
			SetPlayerJump();
			timeStartHold = 0;
		}

	}
		
	public bool IsHold() {
		return hold;
	}

	void SetPlayerWhenStretchLine() {
		player.SetPositionHoldMaxX(player.transform.position.x + 0.5f);
		player.SetState(Player.PlayerState.Stretch);
	}

	void SetPlayerJump() {
		player.SetVec0(player.transform.position);
		player.ResetX();
		player.CreateJumpParabol();
		player.SetState(Player.PlayerState.Jump);
	}

	public void SetGameOver() {
		player.SetState(Player.PlayerState.Die);
		gameOverCanvas.gameObject.SetActive(true);
	}

	public void PlayGameAgain() {
		Application.LoadLevel("MainGame");
	}

	public void ChangeStartFallOnLineValue(bool fallOnLine) {
		startFallOnLine = fallOnLine;
	}





}
