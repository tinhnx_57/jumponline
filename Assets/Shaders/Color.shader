﻿Shader "Custom/Color" {
	Properties{
		_Color("Color ", color) = (1, 1, 1, 1)
		_GrayScaleBonus ("Gray Scale Bonus", float) = 1
	}
	SubShader{
	Tags{ "RenderType" = "Opaque"}
	Pass{
	CGPROGRAM
    #pragma vertex vert 
    #pragma fragment frag

	half4 _Color;
	half _GrayScaleFactor;

	struct VertexInput {
		half4 vertex : POSITION;
	};

	struct VertexOutput {
		half4 pos : SV_POSITION;

	};

	VertexOutput vert(VertexInput i) {
		VertexOutput o;
		o.pos = mul(UNITY_MATRIX_MVP, i.vertex);
		return o;
	}
	
	half _GrayScaleBonus;
	
	half4 frag(VertexOutput i) : COLOR {
		 return _Color;
	}
		ENDCG
	}

	}
		FallBack Off
}